package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation

class TimeDynamicFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time_dynamic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnLimited = view.findViewById<ConstraintLayout>(R.id.cardLimitedTime)
        var btnUnlimited = view.findViewById<ConstraintLayout>(R.id.cardUnlimitedTime)
        btnLimited.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.limitedTimeFragment)
        })
        btnUnlimited.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.cronometerRouteFragment)
        })
    }
}