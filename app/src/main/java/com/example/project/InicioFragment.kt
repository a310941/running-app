package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.Navigation

class InicioFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_inicio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnRegister = view.findViewById<Button>(R.id.btnSignUpInicio)
        var btnLogin = view.findViewById<Button>(R.id.btnLoginInicio)
        btnRegister.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.registrationFragment)
        })
        btnLogin.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.loginFragment)
        })
    }

    override fun onResume() {
        super.onResume()

    }
}