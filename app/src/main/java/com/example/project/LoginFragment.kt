package com.example.project

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnLogin = view.findViewById<Button>(R.id.btnLogin)
        btnLogin.setOnClickListener(View.OnClickListener {

            var email = view.findViewById<EditText>(R.id.edtEmailLogin).text.toString().trim()
            var pass = view.findViewById<EditText>(R.id.edtPasswordLogin).text.toString().trim()
            Log.i("DEBUGAPP", email)
            Log.i("DEBUGAPP",pass)
            auth.signInWithEmailAndPassword(email, pass)
                    .addOnCompleteListener(requireActivity() ) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("DEBUGAPP", "signInWithEmail:success")
                            val user = auth.currentUser
                            Navigation.findNavController(view).navigate(R.id.paginaPrincipalFragment)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("DEBUGAPP", "signInWithEmail:failure", task.exception)
                            Toast.makeText(context, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()
                        }
                    }
            //Navigation.findNavController(view).navigate(R.id.paginaPrincipalFragment)
        })
    }

}
