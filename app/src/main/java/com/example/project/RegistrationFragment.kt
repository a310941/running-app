package com.example.project

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class RegistrationFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnLogin = view.findViewById<Button>(R.id.btnRegisterAccount)
        btnLogin.setOnClickListener(View.OnClickListener {
            var name = view.findViewById<EditText>(R.id.edtRegisterName).text
            var lastName = view.findViewById<EditText>(R.id.edtRegisterName).text
            var email = view.findViewById<EditText>(R.id.edtRegisterEmail).text
            var password = view.findViewById<EditText>(R.id.edtRegisterPassword).text
            var confirmPassword = view.findViewById<EditText>(R.id.edtRegisterConfirmPassword).text


            val completeName = "$name $lastName"

            auth.createUserWithEmailAndPassword(email.toString(), password.toString())
            Navigation.findNavController(view).navigate(R.id.loginFragment);
            //Toast.makeText(getActivity(),"Passwords don't match",Toast.LENGTH_SHORT).show();

    })
    }

}

