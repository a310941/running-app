package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.navigation.Navigation

class CronometerRouteFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cronometer_route, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnPause = view.findViewById<ImageView>(R.id.btnPauseCronometer)
        var btnStop = view.findViewById<ImageView>(R.id.btnStopCronometer)
        btnStop.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.paginaPrincipalFragment)
        })
    }

}