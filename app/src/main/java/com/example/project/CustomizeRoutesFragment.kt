package com.example.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.navigation.Navigation

class CustomizeRoutesFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customize_routes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnWalk = view.findViewById<ImageView>(R.id.btnWalk)
        var btnRun = view.findViewById<ImageView>(R.id.btnRun)
        var btnBike = view.findViewById<ImageView>(R.id.btnBike)
        btnWalk.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.planRouteFragment)
        })
        btnRun.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.planRouteFragment)
        })
        btnBike.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.planRouteFragment)
        })
    }
}