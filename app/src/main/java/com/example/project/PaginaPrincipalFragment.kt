package com.example.project

import android.media.Image
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.navigation.Navigation

class PaginaPrincipalFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pagina_principal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var btnCustomize = view.findViewById<ImageView>(R.id.btnCustomize)
        var btnFastStart = view.findViewById<ImageView>(R.id.btnFastStart)
        btnCustomize.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.customizeRoutesFragment)
        })
        btnFastStart.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view).navigate(R.id.repeatRouteFragment)
        })
    }
}